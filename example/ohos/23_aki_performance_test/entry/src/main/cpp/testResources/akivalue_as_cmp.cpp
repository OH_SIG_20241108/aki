/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include <cstring>
#include "hilog/log.h"
#include "napi/native_api.h"
#include "aki/jsbind.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"
#include "testResources/akivalue_as_cmp.h"

namespace AKITEST_AS {

#define TEST_COUNT 100000
#define TEST_NUMBER 32
#define PI 3.14
#define ZERO 0
#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4
#define FIVE 5
#define SIX 6
#define SEVEN 7
#define EIGHT 8
#define NINE 9
#define TEN 10

template <typename R, typename... args>
struct FunctionInfo {
    R (*func)(args...);
    std::string name;
};

void N_PassingValueAsBool(napi_env env, napi_value value)
{
    bool out;
    napi_get_value_bool(env, value, &out);
}

void N_PassingValueAsUnit8(napi_env env, napi_value value)
{
    uint32_t result32;
    napi_get_value_uint32(env, value, &result32);
    if (result32 <= UINT8_MAX) {
        static_cast<uint8_t>(result32);
    }
}

void N_PassingValueAsInt8(napi_env env, napi_value value)
{
    int32_t result32;
    napi_get_value_int32(env, value, &result32);
    if (result32 <= INT8_MAX) {
        static_cast<int8_t>(result32);
    }
}

void N_PassingValueAsUint16(napi_env env, napi_value value)
{
    uint32_t result32;
    napi_get_value_uint32(env, value, &result32);
    if (result32 <= UINT16_MAX) {
        static_cast<uint16_t>(result32);
    }
}

void N_PassingValueAsInt16(napi_env env, napi_value value)
{
    int32_t result32;
    napi_get_value_int32(env, value, &result32);
    if (result32 <= INT16_MAX) {
        static_cast<int16_t>(result32);
    }
}

void N_PassingValueAsInt(napi_env env, napi_value value)
{
    int32_t result32;
    napi_get_value_int32(env, value, &result32);
}

void N_PassingValueAsInt64(napi_env env, napi_value value)
{
    int64_t result64;
    napi_get_value_int64(env, value, &result64);
    static_cast<int64_t>(result64);
}

void N_PassingValueAsFloat(napi_env env, napi_value value)
{
    double resultDouble;
    napi_get_value_double(env, value, &resultDouble);
    static_cast<float>(resultDouble);
}

void N_PassingValueAsDouble(napi_env env, napi_value value)
{
    double resultDouble;
    napi_get_value_double(env, value, &resultDouble);
    static_cast<double>(resultDouble);
}

void N_PassingValueAsString(napi_env env, napi_value value)
{
    size_t length;
    napi_get_value_string_utf8(env, value, nullptr, ZERO, &length);
    std::string out(length, '\0');
    napi_get_value_string_utf8(env, value, &out[ZERO], length + ONE, &length);
}

void AkiValueAsCmp::NapiValueAsCheckTest(void)
{
    size_t argc = TEN;
    napi_value args[argc];
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_boolean(env, true, &args[ZERO]);
    napi_create_uint32(env, TEST_NUMBER, &args[ONE]);
    napi_create_int32(env, TEST_NUMBER, &args[TWO]);
    napi_create_uint32(env, TEST_NUMBER, &args[THREE]);
    napi_create_int32(env, TEST_NUMBER, &args[FOUR]);
    napi_create_int32(env, TEST_NUMBER, &args[FIVE]);
    napi_create_int32(env, TEST_NUMBER, &args[SIX]);
    napi_create_double(env, PI, &args[SEVEN]);
    napi_create_double(env, PI, &args[EIGHT]);
    napi_create_string_utf8(env, "Hello World", strlen("Hello World"), &args[NINE]);
    FunctionInfo<void, napi_env, napi_value> functionInfos[] = {
        {N_PassingValueAsBool, "AsBool"},     {N_PassingValueAsUnit8, "AsUint8"}, {N_PassingValueAsInt8, "AsInt8"},
        {N_PassingValueAsUint16, "AsUint16"}, {N_PassingValueAsInt16, "AsInt16"}, {N_PassingValueAsInt, "AsInt"},
        {N_PassingValueAsInt64, "AsInt64"},   {N_PassingValueAsFloat, "AsFloat"}, {N_PassingValueAsDouble, "AsDouble"},
        {N_PassingValueAsString, "AsString"}};

    for (int index = ZERO; index < sizeof(functionInfos) / sizeof(FunctionInfo<void, napi_env, napi_value>); index++) {
        std::string functionName = functionInfos[index].name.c_str();
        AKITEST::TestTime().NapiTestTime(TEST_COUNT, "Value",
                                         functionName, functionInfos[index].func, env, args[index]);
    }
}

void PassingValueAsBool(aki::Value& value)
{
    value.As<bool>();
}

void PassingValueAsUint8(aki::Value& value)
{
    value.As<uint8_t>();
}

void PassingValueAsInt8(aki::Value& value)
{
    value.As<int8_t>();
}

void PassingValueAsUint16(aki::Value& value)
{
    value.As<uint16_t>();
}

void PassingValueAsInt16(aki::Value& value)
{
    value.As<int16_t>();
}

void PassingValueAsInt(aki::Value& value)
{
    value.As<int>();
}

void PassingValueAsInt64(aki::Value& value)
{
    value.As<int64_t>();
}

void PassingValueAsFloat(aki::Value& value)
{
    value.As<float>();
}

void PassingValueAsDouble(aki::Value& value)
{
    value.As<double>();
}

void PassingValueAsString(aki::Value& value)
{
    value.As<std::string>();
}

void AkiValueAsCmp::AkiValueAsCheckTest(void)
{
    size_t argc = TEN;
    napi_value args[argc];
    napi_env env = AKITEST::AkiTestManager::GetInstance().GetNapiEnv();
    napi_get_boolean(env, true, &args[ZERO]);
    napi_create_uint32(env, TEST_NUMBER, &args[ONE]);
    napi_create_int32(env, TEST_NUMBER, &args[TWO]);
    napi_create_uint32(env, TEST_NUMBER, &args[THREE]);
    napi_create_int32(env, TEST_NUMBER, &args[FOUR]);
    napi_create_int32(env, TEST_NUMBER, &args[FIVE]);
    napi_create_int32(env, TEST_NUMBER, &args[SIX]);
    napi_create_double(env, PI, &args[SEVEN]);
    napi_create_double(env, PI, &args[EIGHT]);
    napi_create_string_utf8(env, "Hello World", strlen("Hello World"), &args[NINE]);
    aki::Value akiargs[TEN];
    for (int index = ZERO; index < argc; index++) {
        akiargs[index] = args[index];
    }

    FunctionInfo<void, aki::Value &> functionInfos[] = {
        {PassingValueAsBool, "AsBool"},     {PassingValueAsUint8, "AsUint8"}, {PassingValueAsInt8, "AsInt8"},
        {PassingValueAsUint16, "AsUint16"}, {PassingValueAsInt16, "AsInt16"}, {PassingValueAsInt, "AsInt"},
        {PassingValueAsInt64, "AsInt64"},   {PassingValueAsFloat, "AsFloat"}, {PassingValueAsDouble, "AsDouble"},
        {PassingValueAsString, "AsString"}};
    for (int index = ZERO; index < sizeof(functionInfos) / sizeof(FunctionInfo<void, aki::Value &>); index++) {
        std::string functionName = functionInfos[index].name.c_str();
        AKITEST::TestTime().AkiTestTime(TEST_COUNT, "Value", functionName, functionInfos[index].func, akiargs[index]);
    }
}

} // AKITEST_AS