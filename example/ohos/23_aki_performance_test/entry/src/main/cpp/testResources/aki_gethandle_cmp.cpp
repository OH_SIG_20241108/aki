/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "napi/native_api.h"
#include "aki/jsbind.h"
#include "testManager/aki_test_time.h"
#include "testManager/aki_test_manager.h"
#include "testResources/aki_gethandle_cmp.h"

namespace AKITEST_GH {

#define TEST_COUNT 100000

void NapiGetHandleTest(napi_env env, napi_ref ref, napi_value value)
{
    napi_get_reference_value(env, ref, &value);
}

void AkiGetHandleCmp::NapiGetHandleCheckTest(void)
{
    napi_value value;
    napi_ref ref = nullptr;
    napi_env env = aki::JSBind::GetScopedEnv();
    AKITEST::TestTime().NapiTestTime(TEST_COUNT, "GetHandle", "GetHandleTest", NapiGetHandleTest, env, ref, value);
}


void AkiGetHandleTest(aki::Value &value)
{
    value.GetHandle();
}

void AkiGetHandleCmp::AkiGetHandleCheckTest(void)
{
    napi_value napiValue = nullptr;
    aki::Value value(napiValue);
    AKITEST::TestTime().AkiTestTime(TEST_COUNT, "GetHandle", "GetHandleTest", AkiGetHandleTest, value);
}

}